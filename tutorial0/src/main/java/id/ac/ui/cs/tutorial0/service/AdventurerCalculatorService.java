package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    public int countPowerPotentialFromBirthYear(int birthYear);
    public String powerClassifier(int power);
}
